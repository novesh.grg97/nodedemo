/**
* @author NovesH GarG 
*/

const express = require('express');
exports.express = express;

const MongoClient = require('mongodb').MongoClient;
exports.MongoClient = MongoClient;

const bodyParser = require('body-parser');
exports.bodyParser = bodyParser;

const BigNumber = require('big-number');
exports.BigNumber = BigNumber;

const port = 80;
exports.port = port;

const mcache = require('memory-cache');
exports.mcache = mcache;

const app = express();
exports.app = app;
app.use(bodyParser.json());
app.set('view engine', 'ejs');

function checkNotEmpty (data) {
    if (data === null || data === undefined || data === "null" || data === "" || data === " " || data === "undefined" || data === false ) {
        return false;
    } else {
        return true;
    }
}
exports.checkNotEmpty = checkNotEmpty;

const utilityFunc = {
	BASE_62_NUM : "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
	stringStartsWith : function (input, startsWithStr) {
    	if (input == null)
    		return false;
    	if (!(typeof input === 'string'))
    		return false;
    	return input.startsWith(startsWithStr);
    },
    convertToBase62 : function(number) {
		if(number == null) return null;
    	let [dividend, convBldr] = [number, ''];
		while(BigNumber(number).gt(0)) {
			let remainder = BigNumber(number).mod(62);
			number = BigNumber(number).minus(remainder).div(62);
			convBldr = utilityFunc.BASE_62_NUM.charAt(remainder) + convBldr;
		}
		return convBldr;
    },
    convertFromBase62 : function(number) {
    	if (number == null)
    		return null;
    	let posOfLastChar = number.length - 1;
    	return number.split('').reduce((sum, currVal, currIdx) => {
    		return BigNumber(62).pow(posOfLastChar - currIdx).mult(utilityFunc.BASE_62_NUM.indexOf(currVal)).add(sum)
    	}, BigNumber(0))
    }
}
exports.utilityFunc = utilityFunc;

const mongoUser = encodeURIComponent('user');
exports.mongoUser = mongoUser;

const mongoPassword = encodeURIComponent('user');
exports.mongoPassword = mongoPassword;

const mongoDb = 'demo';
exports.mongoDb = mongoDb;

const mongoDbBaseUrl = `mongodb://${mongoUser}:${mongoPassword}@35.200.197.75:27017/demo?authMechanism=DEFAULT&authSource=admin`;
exports.mongoDbBaseUrl = mongoDbBaseUrl;

const mongoose = require('mongoose');
let mongooseDb = mongoose.connect(mongoDbBaseUrl, { useNewUrlParser: true, useUnifiedTopology: true});
exports.mongooseDb = mongooseDb;

let mongoDbClient;
exports.mongoDbClient = mongoDbClient;

function getMongoClient () {
	if (!checkNotEmpty(mongoDbClient)) {
		MongoClient.connect(mongoDbBaseUrl,{ useNewUrlParser: true },function(err, dbClient) {  
    		if (err) {
        		console.error("mongo connection error :",err);
    		}
    		mongoDbClient = dbClient.db(mongoDb);
		});
	}
	return mongoDbClient;
}
exports.getMongoClient = getMongoClient;

let User = require('./DataModels/User.js');
exports.User = User;

let Product = require('./DataModels/Product.js');
exports.Product = Product;

let Category = require('./DataModels/Category.js');
exports.Category = Category;

const cache = (duration) => {
  return (req, res, next) => {
    let key = '_mcache_' + req.originalUrl || req.url;
    if (req.body != null) {
      key += JSON.stringify(req.body);
    }
    
    let cachedBody = mcache.get(key)
    if (cachedBody) {
      res.json(JSON.parse(cachedBody))
      return
    } else {
      res.sendResponse = res.send
      res.send = (body) => {
        mcache.put(key, body, duration * 1000);
        res.sendResponse(body)
      }
      next();
    }
  }
}
exports.cache = cache;


function clean(obj) {
  for (let propName in obj) { 
    if (obj[propName] === null || obj[propName] === undefined) {
      delete obj[propName];
    }
  }
  return obj;
}
exports.clean = clean;





