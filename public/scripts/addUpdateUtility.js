

function addUpdatecategory() {
    
    let useCase = $('input[name="addUpdateRadio"]:checked').val();
    if (!checkNotNull(useCase))
        return toast("Select Add , Update or Delete");

    let categoryNameSelected=document.getElementById("categorySelect").options[document.getElementById("categorySelect").selectedIndex].value;
    let categoryName=document.getElementById("categoryName").value;
    let status = $('input[name="statusRadio"]:checked').val();
    let reqObj = {};

    switch (useCase) {
        case "add":
            if (!checkNotNull(status)) {
                return toast("Kindly select a category status");
            }
            if(!checkNotNull(categoryName) || categoryName.length < 3 || categoryName.length > 30 ){
               return toast("Category Name should be between 3 to 30 characters")
            }
            categoryNameSelected = categoryName;

            reqObj = {name : categoryName, status : status};

            $.ajax({
            "async": true,
            "url": `/category/addUpdateCategory/${categoryNameSelected}`,
            "type": "POST",
            "headers" : {
                    "Content-type" : "application/json"
                },
                "data" : JSON.stringify(reqObj),
                success: function (data) {
                    if (data.error != null) {
                        alert("Error : "+data.error.message);
                        return;
                    }
                    alert("Successful");
                    location.reload();
                },            
                error: function (error) {
                    alert("Error : "+JSON.stringify(error));
                }
            });
        break;

        
        case "update":

            if(!checkNotNull(categoryNameSelected) || categoryNameSelected.length < 3 || categoryNameSelected.length > 30 ){
               return toast("Select a category")
            }

            if(checkNotNull(categoryName) && (categoryName.length < 3 || categoryName.length > 30) ){
               return toast("Category Name should be between 3 to 30 characters")
            }

            if (checkNotNull(categoryName))
                reqObj.name = categoryName;


            if (checkNotNull(status))
                reqObj.status = status;


            $.ajax({
            "async": true,
            "url": `/category/addUpdateCategory/${categoryNameSelected}`,
            "type": "POST",
            "headers" : {
                    "Content-type" : "application/json"
                },
                "data" : JSON.stringify(reqObj),
                success: function (data) {
                    if (data.error != null) {
                        alert("Error : "+data.error.message);
                        return;
                    }
                    alert("Successful");
                    location.reload();
                },            
                error: function (error) {
                    alert("Error : "+JSON.stringify(error));
                }
            });
        break;

        case "delete":

            if (!checkNotNull(categoryNameSelected))
                return toast("Kindly select a category");;

            $.ajax({
            "async": true,
            "url": `/category/deleteCategory/${categoryNameSelected}`,
            "type": "DELETE",
            "headers" : {
                    "Content-type" : "application/json"
                },
                success: function (data) {
                    if (data.error != null) {
                        alert("Error : "+data.error.message);
                        return;
                    }
                    alert("Successful");
                    location.reload();
                },            
                error: function (error) {
                    alert("Error : "+JSON.stringify(error));
                }
            });
        break;
    }        
}







function addUpdateProduct() {

    let useCase = $('input[name="addUpdateRadio"]:checked').val();
    if (!checkNotNull(useCase))
        return toast("Select Add , Update or Delete");

    let regex=/^([0-9])+$/;
     
    let categoryNameSelected=document.getElementById("categorySelect").options[document.getElementById("categorySelect").selectedIndex].value;
    let productNameSelected=document.getElementById("productSelect").options[document.getElementById("productSelect").selectedIndex].value;
    let productName=document.getElementById("productName").value;
    let productDescription=document.getElementById("productDescription").value;
    let productPrice=document.getElementById("productPrice").value;
    let productQuantity=document.getElementById("productQuantity").value;
    let productLink=document.getElementById("productLink").value;

    if (!checkNotNull(categoryNameSelected))
        return toast("Select a category");

    switch (useCase) {
        case "add":
            productNameSelected = productName;
            
            if (!checkNotNull(productNameSelected) || productNameSelected.length < 3 || productNameSelected.length >50)
                return toast("Product Name must be 3 to 50 characters long");

            if (!checkNotNull(productPrice) || !regex.test(productPrice))
                return toast("Product Price must be only numeric");

            if (!checkNotNull(productQuantity) || !regex.test(productQuantity))
                return toast("Product Quantity must be only numeric");

            if (!checkNotNull(productLink))
                return toast("Product Link must be present");

        case "update":

            if (!checkNotNull(productNameSelected))
                return toast("Please select a product");

            let reqObj={
                name : checkNotNull(productName) ? productName : productNameSelected,
                category : categoryNameSelected
            }
            if (checkNotNull(productPrice))
                reqObj.price = productPrice;
            if (checkNotNull(productQuantity))
                reqObj.quantity = productQuantity;
            if (checkNotNull(productLink))
                reqObj.image = productLink;
            if (checkNotNull(productDescription))
                reqObj.description = productDescription;

            $.ajax({
            "async": true,
            "url": `/product/addUpdateProduct/${productNameSelected}`,
            "type": "POST",
            "headers" : {
                    "Content-type" : "application/json"
                },
                "data" : JSON.stringify(reqObj),
                success: function (data) {
                    if (data.error != null) {
                        alert("Error : "+data.error.message);
                        return;
                    }
                    alert("Successful");
                    location.reload();
                },            
                error: function (error) {
                    alert("Error : "+JSON.stringify(error));
                }
            });
        break;

        case "delete":

            if (!checkNotNull(productNameSelected))
                return toast("Select a Product to delete");

            $.ajax({
            "async": true,
            "url": `/product/deleteProduct/${productNameSelected}`,
            "type": "DELETE",
            "headers" : {
                    "Content-type" : "application/json"
                },
                success: function (data) {
                    if (data.error != null) {
                        alert("Error : "+data.error.message);
                        return;
                    }
                    alert("Successful");
                    location.reload();
                },            
                error: function (error) {
                    alert("Error : "+JSON.stringify(error));
                }
            });
        break;
    }        
}


function updateProducts(data) {
    resetFields();
    resetProducts ();
    let category = data.target.value;
    if (!checkNotNull(category)) {
        return toast("Select a Category");
    }

    let reqObj={ category : category }

    $.ajax({
        "async": true,
        "url": `/product/findProductsWithLimits/0/1000`,
        "type": "POST",
        "headers" : {
            "Content-type" : "application/json"
        },
        "data" : JSON.stringify(reqObj),
        success: function (response) {
            if (checkNotNull(response)) {
                response.productData.forEach((item) => {
                    let productSelect = document.getElementById('productSelect');
                    productSelect.options[productSelect.options.length] = new Option(item.name, item.name);
                });
            }
        },            
        error: function (error) {
            alert("Some Error Occured : "+JSON.stringify(error));
        }
    });
}



function updateProductDetails (data) {
    resetFields ();
    let productName = data.target.value;
    if (!checkNotNull(productName)) {
        return toast("Select a Product");
    }

    $.ajax({
        "async": true,
        "url": `/product/findProduct/${productName}`,
        "type": "GET",
        "headers" : {
            "Content-type" : "application/json"
        },
        success: function (response) {
            if (checkNotNull(response)) {
                response.productData.forEach((product) => {

                    document.getElementById('productName').value = product.name;
                    document.getElementById('productDescription').value = product.description;
                    document.getElementById('productQuantity').value = product.quantity;
                    document.getElementById('productPrice').value = product.price;
                    document.getElementById('productLink').value = product.image;
                });
            }
        },            
        error: function (error) {
            alert("Some Error Occured : "+JSON.stringify(error));
        }
    });
}


function resetFields () {
    $('input[type="text"]').val('');
    $('input[type="number"]').val('');
    $('input[type="textArea"]').val('');
}



function toggleDisabled(className, disableState){
    let elements = document.getElementsByClassName(className);
    for (let i = 0; i < elements.length; i++){
        elements[i].disabled = disableState;
    }
}


function resetProducts () {
    $('#productSelect')
    .find('option')
    .remove()
    .end()
    .append('<option value="">--Please choose an option if you want to Update --</option>')
    .val('');
}



function enableFields(data) {
    switch (data.target.value) {
        case "add":
            toggleDisabled("update", true);
            toggleDisabled("delete", true);
            toggleDisabled("add", false);            
        break;
        case "update":
            toggleDisabled("add", true);
            toggleDisabled("delete", true);
            toggleDisabled("update", false);
        break;
        case "delete":
            toggleDisabled("add", true);
            toggleDisabled("update", true);
            toggleDisabled("delete", false);
        break;
    }
}


















