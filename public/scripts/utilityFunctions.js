
function checkNotNull (data) {
    if (data == null || data == "null" || data == "" || data == " " || data == "undefined" || data === false ) {
        return false;
    } else {
        return true;
    }
}


function toBaseN(num, base) {
  if (num === 0) {
    return '0';
  }
  var digits = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var len = Math.min(digits.length, base);
  var result = ''; 
  while (num > 0) {
    result = digits[num % len] + result;
    num = parseInt(num / len, 10);
  }
  
  return result;
}


function toBase62(n) {
  if (n === 0) {
    return '0';
  }
  var digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  var result = ''; 
  while (n > 0) {
    result = digits[n % digits.length] + result;
    n = parseInt(n / digits.length, 10);
  }
  
  return result;
}

function fromBase62(s) {
  var digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  var result = 0;
  for (var i=0 ; i<s.length ; i++) {
    var p = digits.indexOf(s[i]);
    if (p < 0) {
      return NaN;
    }
    result += p * Math.pow(digits.length, s.length - i - 1);
  }
  return result;
}

var toast = message => {
      var elem = document.getElementById("toast");
      elem.getElementsByTagName("p")[0].innerText = message;
      elem.style.visibility = "visible";
      setTimeout(() => {
        elem.style.visibility = "hidden";
      }, 10000);
    };
