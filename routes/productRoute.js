const constants = require('./../constants.js');

const Product = constants.Product;

module.exports = function(app, db) {
  app.get('/product/findProducts', constants.cache(10), (req, res) => {
  	Product.read({}, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              productData: productData
          })
        }
    });
  });

  app.post('/product/findProductsWithLimits/:offset/:limit', constants.cache(10), (req, res) => {
    let productDataReceived = req.body;
    Product.readWithLimits(productDataReceived, req.params.offset, req.params.limit, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              productData: productData
          })
      }
    });
  });

  app.get('/product/findProduct/:name', constants.cache(10), (req, res) => {
  	Product.read({name: req.params.name}, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              productData: productData
          })
        }
    });
  });

  app.post('/product/createProduct', (req, res) => {
  	let productDataReceived = req.body;
  	Product.create(productDataReceived, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              productData: productData
          })
        }
    });
  });

  app.put('/product/updateProduct/:name', (req, res) => {
  	let productDataReceived = req.body;
  	Product.update({name: req.params.name}, productDataReceived, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              productData: productData
          })
        }
    });
  });

  app.delete('/product/deleteProduct/:name', (req, res) => {
  	Product.delete({name: req.params.name}, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              productData: productData
          })
        }
    });
  });


  app.post('/product/addUpdateProduct/:name', (req, res) => {
    let productDataReceived = constants.clean(req.body);
    Product.read({name: req.params.name}, function(error, productData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          if (productData == null || productData.length == 0) {
            Product.create(productDataReceived, function(error, productData) {
              if(error) {
                res.json({
                  error: error
                })
              } else {
                res.json({
                    productData: productData
                })
              }
            });
          } else {
            Product.update({name: req.params.name}, productDataReceived, function(error, productData) {
              if(error) {
                res.json({
                  error: error
                })
            } else {
              res.json({
                productData: productData
              })
            }
          });
        }
        }
    });
  });

};

