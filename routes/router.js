const userRoute = require('./userRoute.js');
const categoryRoute = require('./categoryRoute.js');
const productRoute = require('./productRoute.js');
const authorizationRoute = require('./authorizationRoute.js');

const viewRoutes = require('./views/viewRoutes.js');

module.exports = function(app, db) {
  userRoute(app, db);
  categoryRoute(app, db);
  productRoute(app, db);
  authorizationRoute(app, db);
  viewRoutes(app, db);
};