const constants = require('./../constants.js');

const User = constants.User;

module.exports = function(app, db) {
  app.get('/user/findUsers', constants.cache(10), (req, res) => {
  	User.read({}, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
	        res.json({
	            userData: userData
	        })
	    }
    });
  });

  app.get('/user/findUsersWithLimits/:offset/:limit', constants.cache(10), (req, res) => {
  	User.readWithLimits({}, req.params.offset, req.params.limit, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
	        res.json({
	            userData: userData
	        })
	    }
    });
  });

  app.get('/user/findUser/:userName', constants.cache(10), (req, res) => {
  	User.read({userName: req.params.userName}, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
	        res.json({
	            userData: userData
	        })
	    }
    });
  });

  app.post('/user/createUser', (req, res) => {
  	let userDataReceived = req.body;
  	User.create(userDataReceived, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
	        res.json({
	            userData: userData
	        })
	    }
    });
  });

  app.put('/user/updateUser/:userName', (req, res) => {
  	let userDataReceived = req.body;
  	User.update({userName: req.params.userName}, userDataReceived, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
	        res.json({
	            userData: userData
	        })
	    }
    });
  });

  app.delete('/user/deleteUser/:userName', (req, res) => {
  	User.delete({userName: req.params.userName}, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
	        res.json({
	            userData: userData
	        })
	    }
    });
  });


  app.post('/user/addUpdateUser/:userName', (req, res) => {
    let userDataReceived = constants.clean(req.body);
    User.read({userName: req.params.userName}, function(error, userData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          if (userData == null || userData.length == 0) {
            User.create(userDataReceived, function(error, userData) {
              if(error) {
                res.json({
                  error: error
                })
              } else {
                res.json({
                    userData: userData
                })
              }
            });
          } else {
            console.log("\n\n Updating");
            console.log(JSON.stringify(userDataReceived));
            User.update({userName: req.params.userName}, userDataReceived, function(error, userData) {
              if(error) {
                res.json({
                  error: error
                })
            } else {
              res.json({
                userData: userData
              })
            }
          });
        }
        }
    });
  });

};

