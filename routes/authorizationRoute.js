const constants = require('./../constants.js');

const User = constants.User;

module.exports = function(app, db) {
  app.post('/auth/validateLogin/:userName', (req, res) => {
    let responseJson = {
      responseCode: 404,
      message: "Invalid Login Details!"
    }
    let passwordReceived = constants.utilityFunc.convertFromBase62(req.body.password);
  	User.read({userName: req.params.userName, password: passwordReceived}, function(error, userData) {
        if(error) {
            res.json(responseJson);
        } else {
          if (constants.checkNotEmpty(userData) && userData.length > 0) {
            responseJson.responseCode = 200;
            responseJson.message = "Login Successful!",
            responseJson.user = userData[0]
          }
          res.json(responseJson);
	    }
    });
  });
};

