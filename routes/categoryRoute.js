const constants = require('./../constants.js');

const Category = constants.Category;

module.exports = function(app, db) {
  app.get('/category/findCategorys', constants.cache(10), (req, res) => {
  	Category.read({}, function(error, categoryData) {
        if(error) {
            res.json({
                error: error,
                stack : error.stack
            })
        } else {
          res.json({
              categoryData: categoryData
          })
        }
    });
  });

  app.post('/category/findCategorysWithLimits/:offset/:limit', constants.cache(10), (req, res) => {
    let categoryDataReceived = req.body;
    Category.readWithLimits(categoryDataReceived, req.params.offset, req.params.limit, function(error, userData) {
        if(error) {
            res.json({
                error: error,
                stack : error.stack
            })
        } else {
          res.json({
              categoryData: userData
          })
      }
    });
  });

  app.get('/category/findCategory/:name', constants.cache(10), (req, res) => {
  	Category.read({name: req.params.name}, function(error, categoryData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              categoryData: categoryData
          })
        }
    });
  });

  app.post('/category/createCategory', (req, res) => {
  	let categoryDataReceived = req.body;
  	Category.create(categoryDataReceived, function(error, categoryData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              categoryData: categoryData
          })
        }
    });
  });

  app.post('/category/addUpdateCategory/:name', (req, res) => {
    let categoryDataReceived = constants.clean(req.body);
    Category.read({name: req.params.name}, function(error, categoryData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          if (categoryData == null || categoryData.length == 0) {
            Category.create(categoryDataReceived, function(error, categoryData) {
              if(error) {
                res.json({
                  error: error
                })
              } else {
                res.json({
                    categoryData: categoryData
                })
              }
            });
          } else {
            Category.update({name: req.params.name}, categoryDataReceived, function(error, categoryData) {
              if(error) {
                res.json({
                  error: error
                })
            } else {
              res.json({
                categoryData: categoryData
              })
            }
          });
        }
        }
    });
  });
    

  app.put('/category/updateCategory/:name', (req, res) => {
  	let categoryDataReceived = req.body;
  	Category.update({name: req.params.name}, categoryDataReceived, function(error, categoryData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              categoryData: categoryData
          })
        }
    });
  });

  app.delete('/category/deleteCategory/:name', (req, res) => {
  	Category.delete({name: req.params.name}, function(error, categoryData) {
        if(error) {
            res.json({
                error: error
            })
        } else {
          res.json({
              categoryData: categoryData
          })
        }
    });
  });
};

