const constants = require('./../../constants.js');

module.exports = function(app, db) {

	constants.app.get('/',  (req, res) => {
		res.render('index');
	});
	

	app.get('/home/:userName/:userType', function(req, res) {

		console.log(JSON.stringify(req.params));

		let userName = constants.utilityFunc.convertToBase62(req.params.userName);
		let userType = constants.utilityFunc.convertToBase62(req.params.userType);

		let filter = {};

		if (userType == "user")
			filter.status = "active";

		let data = {data : {userName : userName, categories : null, userType : userType, userNameEn : req.params.userName, userTypeEn : req.params.userType}};
		constants.Category.readWithLimits(filter, req.params.offset, req.params.limit, function(error, categoryData) {
			if(error) {
            	console.log(error)
        	} else {
          		data.data.categories = categoryData;
      		}
      		console.log(JSON.stringify(data));
      		res.render('pages/home', data);
		});
	});

	app.get('/home/:userName/:userType/:category/:offset/:limit', function(req, res) {
		let userName = constants.utilityFunc.convertToBase62(req.params.userName);
		let userType = constants.utilityFunc.convertToBase62(req.params.userType);

		let category = req.params.category;
		let offset = req.params.offset;
		let limit = req.params.limit;

		console.log(JSON.stringify(req.params));

		let data = {data : {userName : userName, categories : null, offset : offset, limit : limit, category : category, userType : userType, userNameEn : req.params.userName, userTypeEn : req.params.userType}};

		let categoryFilter = {};
		if (userType == "user")
			categoryFilter.status = "active";

		constants.Category.readWithLimits(categoryFilter, 0, null, function(error, categoryData) {
			if(error) {
            	console.log(error)
        	} else {
          		data.data.categories = categoryData;
          		let productFilter = {category : category};
          		console.log(JSON.stringify(productFilter));
          		constants.Product.readWithLimits(productFilter, offset, limit, function(error, productData) {
          			if(error) {
		            	console.log(error)
		        	} else {
		          		data.data.products = productData;
		      		}
		      		console.log(JSON.stringify(data));
      				res.render('pages/home', data);
          		});
      		}
		});
	});
};






