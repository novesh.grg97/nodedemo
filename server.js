const constants = require('./constants.js');
require('./routes/router.js')(constants.app, {});

constants.app.use('/static', constants.express.static('public'));

constants.app.listen(constants.port, () => {  
	console.log('Server Started ');
});