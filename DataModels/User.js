const mongoose = require('mongoose');
let Schema = mongoose.Schema;

var userSchema = new Schema({
  name: {
    type: String,
    trim: true,
    minlength: 3,
    maxlength: 20,
    required: true
  },
  userName: {
    type: String,
    lowercase: true,
    trim: true,
    minlength: 5,
    maxlength: 20,
    required: true
  },
  userType: {
    type: String,
    trim: true,
    lowercase: true,
    enum: ['user', 'admin'],
    required: true
  },
  password: {
    type: String,
    minlength: 6,
    maxlength: 6,
    required: true
  },
  updated: { 
    type: Date, 
    default: Date.now() 
  }
});

userSchema.statics = {

  create : function(data, callback) {
    let user = new this(data);
    user.save(callback);
  },

  read: function(query, callback) {
    this.find(query, callback);
  },

  update: function(query, updateData, callback) {
    this.findOneAndUpdate(query, {$set: updateData},{new: true}, callback);
  },

  delete: function(query, callback) {
    this.findOneAndDelete(query,callback);
  },

  readWithLimits: function(query, offset, limit, callback) {
    this.find(query, null, {skip: parseInt(offset), limit: parseInt(limit)}, callback);
  }
}
module.exports = mongoose.model('user', userSchema );



