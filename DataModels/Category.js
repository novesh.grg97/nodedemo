const mongoose = require('mongoose');
let Schema = mongoose.Schema;

var categorySchema = new Schema({
  name: {
    type: String,
    minlength: 3,
    maxlength: 30,
    trim: true,
    required: true
  },
  status: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    enum: ['active', 'inactive'],
  },
  updated: { 
    type: Date, 
    default: Date.now() 
  }
});

categorySchema.statics = {

  create : function(data, callback) {
    let category = new this(data);
    category.save(callback);
  },

  read: function(query, callback) {
    this.find(query, callback);
  },

  update: function(query, updateData, callback) {
    this.findOneAndUpdate(query, {$set: updateData},{new: true}, callback);
  },

  delete: function(query, callback) {
    this.findOneAndDelete(query,callback);
  },

  readWithLimits: function(query, offset, limit, callback) {
    this.find(query, null, {skip: parseInt(offset), limit: parseInt(limit)}, callback);
  }
}
module.exports = mongoose.model('categories', categorySchema );