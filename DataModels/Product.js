const mongoose = require('mongoose');
let Schema = mongoose.Schema;

var productSchema = new Schema({
  name: {
    type: String,
    trim: true,
    minlength: 3,
    maxlength: 50,
    required: true
  },
  image: {
    type: String,
    trim: true,
    minlength: 3,
    maxlength: 200
  },
  description: {
    type: String,
    minlength: 3,
    maxlength: 500
  },
  price: {
    type: Number,
    min:1,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  category: {
    type: String,
    trim: true,
    required: true
  },
  updated: { 
    type: Date, 
    default: Date.now() 
  }
});

productSchema.statics = {

  create : function(data, callback) {
    let product = new this(data);
    product.save(callback);
  },

  read: function(query, callback) {
    this.find(query, callback);
  },

  update: function(query, updateData, callback) {
    this.findOneAndUpdate(query, {$set: updateData},{new: true}, callback);
  },

  delete: function(query, callback) {
    this.findOneAndDelete(query,callback);
  },

  readWithLimits: function(query, offset, limit, callback) {
    this.find(query, null, {skip: parseInt(offset), limit: parseInt(limit)}, callback);
  }
}
module.exports = mongoose.model('products', productSchema );


